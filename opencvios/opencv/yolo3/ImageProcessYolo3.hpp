//
//  ImageProcessYolo3.hpp
//  opencvios
//
//  Created by Eugene Golovanov on 5/03/19.
//  Copyright © 2019 Eugene Golovanov. All rights reserved.
//

#ifndef ImageProcessYolo3_hpp
#define ImageProcessYolo3_hpp

#include <fstream>
#include <sstream>
#include <iostream>

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/types_c.h>

class ImageProcessYolo3 {
    private:
        cv::dnn::Net net;
        std::string path;
    public:
        ImageProcessYolo3(std::string path);
    
        // Yolo3 inferencing
        cv::Mat detectYolo3(cv::Mat src);
};

#endif /* ImageProcessYolo3_hpp */
